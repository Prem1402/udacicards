import React from "react";
import { View, Text, TouchableOpacity } from "react-native";

export default function ActionButton({ style, text, color, onPress }) {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[style.iosBtn, { backgroundColor: color }]}
    >
      <Text 
      style={style.submitBtnText}
      >{text}</Text>
    </TouchableOpacity>
  );
}
