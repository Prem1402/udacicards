import { AsyncStorage } from 'react-native'

const FLASHCARDS_STORAGE_KEY = 'decks'

const initialData = {
    React: {
        title: 'React',
        questions: [
            {
                question: 'What is React?',
                answer: 'A library for managing user interfaces'
            },
            {
                question: 'Where do you make Ajax requests in React?',
                answer: 'The componentDidMount lifecycle event'
            }
        ]
    },
    JavaScript: {
        title: 'JavaScript',
        questions: [
            {
                question: 'What is a closure?',
                answer: 'The combination of a function and the lexical environment within which that function was declared.'
            }
        ]
    }
}

export const getData = () => {
    return initialData
}

export function getDecks() {
    return AsyncStorage.getItem(FLASHCARDS_STORAGE_KEY)
        .then(
            result => {
                if (result === null) {
                    AsyncStorage.setItem(FLASHCARDS_STORAGE_KEY, JSON.stringify(initialData))
                    return initialData
                } else {
                    return JSON.parse(result)
                }
            }
        ).catch(function (error) {
            console.log(error);
        })

}

export function saveDeckTitle(title) {
    return AsyncStorage.mergeItem(FLASHCARDS_STORAGE_KEY, JSON.stringify({
        [title]: {
            title: title,
            questions: []
        }
    }))
}

export function addCardToDeck(name, card) {
    return AsyncStorage.getItem(FLASHCARDS_STORAGE_KEY).then(results => JSON.parse(results)).then(results => {
        results[name].questions.push(card)
        AsyncStorage.setItem(FLASHCARDS_STORAGE_KEY, JSON.stringify(results))
        return results
    })
}