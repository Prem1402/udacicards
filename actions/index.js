export const ADD_DECK = "ADD_DECK";
export const RECEIVE_DECKS = "RECEIVE_DECKS";
export const ADD_CARD_TO_DECK = "ADD_CARD_TO_DECK";

export const addDeck = deck => ({
  type: ADD_DECK,
  deck
});

export const receiveDecks = (decks) => ({
  type: RECEIVE_DECKS, 
  decks
})

export const addCard = (card) => ({
  type: ADD_CARD_TO_DECK, 
  card
})
